const button = document.querySelector(".btn");
const containerSet = document.querySelector(".input-wrapper");
const containerConf = document.querySelector(".input-2");
let input = document.querySelectorAll(".password");
let mismatch = document.querySelector(".error");

containerSet.addEventListener("click", passwordHandler);
containerConf.addEventListener("click", passwordHandler);
button.addEventListener("click", confirmation);

function passwordHandler(e) {
  let item = e.target;
  console.log(e.target);
  if (item.classList.contains("icon-password")) {
    let password = item.parentElement.querySelector(".password");
    item.classList.toggle("fa-eye-slash");
    item.classList.toggle("fa-eye");
    if (password.type === "password") {
      password.type = "text";
    } else {
      password.type = "password";
    }
  }
}

function confirmation(e) {
  if (input[0].value === input[1].value) {
    alert("You are welcome");
    input.forEach((e) => (e.value = ""));
  } else {
    mismatch.innerText = "Нужно ввести одинаковые значения!";
    mismatch.style.color = "red";
  }
}
